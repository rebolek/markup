Red[]

#include %XML.red

; -- testing


count: 6

foreach format [triples compact key-val] [

	repeat index count [
		raw: read rejoin [%data/test- index %.xml]
		data: to-xml load-xml raw
		result: equal? data to-xml/as load-xml/as data format format
		print [format index result]
	]

]
